// CREATING DATASET OF TASKS FROM localStorage IF EXIST

var data = (localStorage.getItem('todoList'))
    ? JSON.parse(localStorage.getItem('todoList'))
    : [];

var dataDone = (localStorage.getItem('todoListDone'))
    ? JSON.parse(localStorage.getItem('todoListDone'))
    : [];



// CREATING EXISTED TASK LISTS FROM localStorage

renderTodoLists(data, 'todo-list');
renderTodoLists(dataDone, 'todo-list-done');



// ADDING LISTENERS TO ADD NEW TASK FROM INPUT

document.getElementById('btn-create').addEventListener('click', addNewTask);
document.getElementById('input-create').addEventListener('keydown', (e) => {
    if (event.which == 13 || event.keyCode == 13) {
        addNewTask();
    }
    return false;
});



// ADD NEW TASK FROM INPUT

function addNewTask() {
    var value = document.getElementById('input-create').value;	// Parsing value of input

    if (value) {
        data.push(value);										// Adding value to 'data'
        addItemToDOM(value, 'todo-list');						// Adding value into DOM
        document.getElementById('input-create').value = '';		// Cleaning input from value
    }
}



// RENDER NEW TASK FROM DATASETS

function renderTodoLists(dataType, listType) {
    var list = document.getElementById(listType);	// Finding list of tasks in DOM
    list.innerHTML = "";							// Cleaning it

    if (!dataType.length) return;					// If dataset is empty - do nothing

    for (var i = 0; i < dataType.length; i++) {		// Constructing list of tasks from dataset
        var value = dataType[i];
        addItemToDOM(value, listType);
    }
}



// ADD NEW TASK FROM INPUT TO DOM

function addItemToDOM(text, listType) {
    var list = document.getElementById(listType);	// Finding list of tasks in DOM

    // Creating a new task element
    var item = document.createElement('li');
    item.classList = `collection-item ${listType}`;
    var listItemView = `
    <div class="item">
        <span class="item-text grey-text text-darken-2 ${listType}-text">${text}</span>
        <span class="secondary-content">
            <div class="btn item-btn item-btn-del btn-floating btn-small waves-effect waves-light teal lighten-2 ${listType}-btn">
                ✓
            </div>
        </span>
    </div>`;
    item.innerHTML = listItemView;

    // If 'done' button is pressed - remove task element from 'Active'
    item.getElementsByClassName('item-btn-del')[0].addEventListener('click', removeItem);

    // Animation of task sliding in list
    item.classList.add('in');
    list.insertBefore(item, list.firstChild);
    setTimeout(() => {
      item.classList.remove('in');
    }, 300);

    // Editing active task on double click
    item.addEventListener('dblclick', (e) => {
        var target = item.querySelector('.todo-list-text');

        var targetValue = target.innerHTML;		// Getting existed value of task
        var index = data.indexOf(targetValue);	// Getting index of value from dataset

        if (index == -1) return false;			// Preventing double clicks while editing (if value is empty - editing in process)

        target.innerHTML =						// Replace task value with editing input field
        `<input id="task-editor" type="text" class="validate" value="${targetValue}">`;

        // Applying edit changes by pressing 'Enter'
        target.firstChild.onkeydown = (e) => {
            if (event.which == 13 || event.keyCode == 13) {
                if (target.firstChild.value.length != 0) {
                    data[index] = target.firstChild.value;
                    target.innerHTML = target.firstChild.value;
                    storageSync();
                } else {
                    alert('Task must have at least one symbol!');
                }
            }
        }
    });

    storageSync();	// Sync new information with localStorage

    return item;	// Returning NODE if needed
}



// REMOVING TASK FROM ACTIVE LIST

function removeItem(e) {

    var item = this.parentNode.parentNode.parentNode;
    var list = item.parentNode;
    var value =												// Getting value from deleting task
    item.getElementsByClassName('item-text')[0].innerText;

    if (value.length == 0) {								// Prevention of deleting task during editing
        alert('You can\'t complete the task during editing!' );
        return false;
    }

    data.splice(data.indexOf(value), 1);					// Sync new information with localStorage

    // Animation of deleting task sliding out
    item.classList.add('out');
    setTimeout(() => {
        list.removeChild(item);
    }, 500);

    dataDone.push(value);      								// Adding deleted task to 'dataDone' dataset
    addItemToDOM(value, 'todo-list-done');      			// Rendering new 'Done' task in DOM
}




// SEARCH QUERY FROM TO-DO DONE

var search = document.querySelector('#input-done-search');	// Finding searching input in DOM

search.addEventListener('input', (e) => {					// 'LIVE' search - changing results while tapping
	var resultsList = [];

	for (let i = 0; i < dataDone.length; i++) {				// Searching in 'dataDone' from input query
		if (dataDone[i].toLowerCase().indexOf(search.value.toLowerCase()) != -1) {
			resultsList.push(dataDone[i]);
		}
	}

	renderTodoLists(resultsList, 'todo-list-done');			// Reconstructing 'Done' list with new data
});



// SYNC OF localStorage WITH data AND dataDone

function storageSync() {
    localStorage.setItem('todoList', JSON.stringify(data));
    localStorage.setItem('todoListDone', JSON.stringify(dataDone));
}